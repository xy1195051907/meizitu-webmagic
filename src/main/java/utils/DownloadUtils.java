package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author xbl
 * @date 2018/9/16 14:34
 */
public class DownloadUtils {
    private static final Logger logger = LoggerFactory.getLogger(DownloadUtils.class.getName());
    private DownloadUtils() {
    }
    public static void downloadImg(String title, List<String> urls, List<String> nowUrls){
        DownUtil downUtil = null;
        String suffix = PathUtils.getSuffix(urls.get(0));
        for (int i = 0; i < urls.size(); i++) {
            if (urls.get(i)==null){
                continue;
            }
            String targetFile = "D:\\迅雷下载\\妹子图\\"+title+"\\"+i+"."+suffix;
            downUtil = new DownUtil(urls.get(i), targetFile, 3);
            try {
                downUtil.downLoad(nowUrls.get(i));
            } catch (Exception e) {
                logger.info("targetFile:"+targetFile+",url:"+urls.get(i)+",Referer:"+nowUrls.get(i));
                e.printStackTrace();
            }
        }
    }
}
