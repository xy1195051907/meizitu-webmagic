package utils;

/**
 * @author xbl
 * @date 2018/9/16 14:43
 */
public class PathUtils {
    private PathUtils() {
    }

    public static String getSuffix(String url){
        if (url==null){
            return "";
        }
        String[] split1 = url.split("/");
        String img = split1[split1.length - 1];
        String[] split2 = img.split("\\.");
        return split2[split2.length - 1];
    }

    public static String getDirectory(String url){
        String[] split = url.split("\\\\");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < split.length-1; i++) {
            stringBuilder.append(split[i]+"\\");
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        String suffix = getSuffix("http://i.meizitu.net/2018/09/16a01.jpg");
        String directory = getDirectory("http://i.meizitu.net/2018/09/16a01.jpg");
        System.out.println(suffix);
        System.out.println(directory);
    }
}
