package pipeline;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import utils.DownloadUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author xbl
 * @date 2018/9/16 14:30
 */
public class MyPipeLine implements Pipeline {

    @Override
    public void process(ResultItems resultItems, Task task) {
        HashSet<String> imgs = resultItems.get("imgs");
        List<String> nowUrls = resultItems.get("nowUrls");
        List<String> imgsList =null;
        if (imgs!=null){
            imgsList = new ArrayList<>(imgs);
        }
        String title = resultItems.get("title");
        if (imgs!=null && title!=null && nowUrls!=null){
            DownloadUtils.downloadImg(title,imgsList,nowUrls);
        }
    }
}
